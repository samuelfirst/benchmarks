#!/bin/bash

# Copyright (C) 2019 Samuel First
# This file is part of benchmarks.

#     benchmarks is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     benchmarks is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.

# Get tree of repo
tree="$(tree ..)"

# Write out tree to file
echo "$tree" > index.txt

# Get list of file types and count of each
files=$(grep -oE '\..*' <<< "$tree" | sort | uniq -c | tail -n +2)

# Write stats on number of files to dat file
awk '{print NR-1"\t"$2"\t"$1}' <<< "$files" > file-percentage.dat

# Generate Bar Chart of Number of Files w/ gnuplot
gnuplot file-percentage.plt
