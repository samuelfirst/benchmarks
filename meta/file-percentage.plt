set term png
set output 'file-percentage.png'
set boxwidth 0.5
set yrange[0:]
set style fill solid
set ylabel "Number of Occurences"
set xlabel "File Extension"
set title "Occurences Per File Type"
plot "file-percentage.dat" using 1:3:($1+1):xtic(2) with boxes linecolor variable title ""
