#!/usr/bin/python3
'''
 Copyright (C) 2019 Samuel First
 This file is part of benchmarks.

     benchmarks is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     benchmarks is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.
'''

import argparse

# Get iterations from command line 
parser = argparse.ArgumentParser()
parser.add_argument('iterations',
                    type=int,
                    help='Number of times to run sequence')
args = parser.parse_args()

# The fibonacci sequence starts with (0, 1), and adds the previous number
# to the current number, like 0, 1, 2, 3, 5, 8, 13, 21...

preNum = 0
curNum = 1

for i in range(1, args.iterations):
    fib = preNum + curNum
    preNum = curNum
    curNum = fib

print(curNum)
