#!/bin/bash

# Copyright (C) 2019 Samuel First
# This file is part of benchmarks.

#     benchmarks is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     benchmarks is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.

# First arg is the number of times to run the command
# Second arg is the number of iterations to pass the command
timesToRun=$1
iterations=$2

commands[1]='python3 fibonacci.py'
commands[2]='clisp fibonacci.lisp'
commands[3]='bash fibonacci.sh'
commands[4]='ruby fibonacci.rb'

for n in $(seq 1 ${#commands[@]})
do
    averageMem='0'
    for i in $(seq 1 $timesToRun)
    do
	command=$((/usr/bin/time -f "%M" $(sed -E "s/'//g" <<< "${commands[n]}") $iterations) 2>&1)
	mem=$(awk -F '\n' 'BEGIN{RS=""}{print $2}' <<< "$command")
	let "averageMem += $mem"
    done
    printf "%s: %s\n" "${commands[n]}" $(awk '{print ($1/$2)}' <<< $averageMem' '$timesToRun)
done
