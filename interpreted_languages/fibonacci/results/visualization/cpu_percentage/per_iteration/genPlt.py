#!/usr/bin/python3
'''
 Copyright (C) 2019 Samuel First
 This file is part of benchmarks.

     benchmarks is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     benchmarks is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.
'''

import argparse

# Get file names from command line
parser = argparse.ArgumentParser()
parser.add_argument('input',
                    type=str,
                    help='Input file')
args = parser.parse_args()

# Generate gnuplot script to create bar chart w/ different bar colors for
# each language
with open(f'{args.input.rsplit(".")[0]}.plt', 'w') as f:
    f.write('set term png\n')
    f.write(f'set output "charts/{args.input.rsplit(".")[0]}.png"\n')
    f.write('set boxwidth 0.5\n')
    f.write('set yrange [0:100]\n')
    f.write('set linetype 1 lc rgb "blue"\n')
    f.write('set linetype 2 lc rgb "red"\n')
    f.write('set linetype 3 lc rgb "green"\n')
    f.write('set style fill solid\n')
    f.write('set ylabel "CPU Usage(%)"\n')
    f.write('set xlabel "Iterations"\n')
    f.write('set title "Fibonacci CPU Usage"\n')
    f.write(f'plot "{args.input}" using 1:3:($1+1):xtic(2) with boxes ' +
            'linecolor variable title ""')
