set term png
set output "charts/bash.png"
set boxwidth 0.5
set yrange [0:100]
set style fill solid
set ylabel "CPU Usage(%)"
set xlabel "Iterations"
set title "Fibonacci CPU Usage"
plot "bash.dat" using 1:3:xtic(2) with boxes title ""