;;;; Copyright (C) 2019 Samuel First
;;;; This file is part of benchmarks.
;;;;
;;;;     benchmarks is free software: you can redistribute it and/or modify
;;;;     it under the terms of the GNU General Public License as published by
;;;;     the Free Software Foundation, either version 3 of the License, or
;;;;     (at your option) any later version.
;;;; 
;;;;     benchmarks is distributed in the hope that it will be useful,
;;;;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;     GNU General Public License for more details.
;;;; 
;;;;     You should have received a copy of the GNU General Public License
;;;;     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.

(doList (arg *args*)
  (defvar *iterations* (parse-integer arg)))

;; The fibonacci sequence starts with (0, 1), and adds the previous number
;; to the current number, like 0, 1, 2, 3, 5, 8, 13, 21...

(defvar *pre-num* 0)
(defvar *cur-num* 1)

(loop for i from 2 to *iterations* doing
     (let ((fib (+ *pre-num* *cur-num*)))
       (setf *pre-num* *cur-num*)
       (setf *cur-num* fib)))

(format t "~A" *cur-num*)
