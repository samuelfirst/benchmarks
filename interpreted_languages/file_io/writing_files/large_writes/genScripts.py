#!/usr/bin/python3
'''
 Copyright (C) 2019 Samuel First
 This file is part of benchmarks.

     benchmarks is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
 
     benchmarks is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.
'''

import argparse

# Get size from command line 
parser = argparse.ArgumentParser()
parser.add_argument('size',
                    type=int,
                    help='Number of times to run sequence')
args = parser.parse_args()

# Write out Python Script
with open('writing_files.py', 'w') as f:
    f.write(f'data="{"x"*args.size}"\n')
    f.write('with open("file", "w") as f:\n')
    f.write('\tf.write(data)\n')

# Write out Common Lisp Script
with open('writing_files.lisp', 'w') as f:
    f.write(f'(defvar *data* "{"x"*args.size}")\n')
    f.write('''(with-open-file (stream "file" 
                        :direction :output 
                        :if-exists :supersede)
  (write-line *data* stream))''')

# Write out Bash Script
with open('writing_files.sh', 'w') as f:
    f.write(f'data="{"x"*args.size}"\n')
    f.write('printf "%s" "$data" > ./file')
