set boxwidth 0.5
set yrange [0:]
set linetype 1 lc rgb "blue"
set linetype 2 lc rgb "red"
set linetype 3 lc rgb "green"
set style fill solid
set ylabel "Time (Seconds)"
set xlabel "Language"
set title "Large Writes Runtime: Python, Bash, and Common Lisp"
plot "1000_bytes.dat" using 1:3:($1+1):xtic(2) with boxes linecolor variable title ""