set boxwidth 0.5
set yrange [0:]
set linetype 1 lc rgb "blue"
set linetype 2 lc rgb "red"
set linetype 3 lc rgb "blue"
set linetype 4 lc rgb "red"
set linetype 5 lc rgb "blue"
set linetype 6 lc rgb "red"
set style fill solid
set ylabel "Time (Seconds)"
set xlabel "Language"
set title "Large Writes Runtime: User vs. Sys"
plot "user_sys_100.dat" using 1:3:($1+1):xtic(2) with boxes linecolor variable title "", (x=-1) with boxes lc rgb "blue" t "User", (x=-2) with boxes lc rgb "red" t "Sys"