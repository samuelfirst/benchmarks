#!/usr/bin/python3
'''
Copyright (C) 2019 Samuel First
This file is part of benchmarks.

    benchmarks is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    benchmarks is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.
'''
import argparse

# Get iterations from command line arg
parser = argparse.ArgumentParser()
parser.add_argument('iterations',
                    type=int,
                    help='Number of times to run sequence')
args = parser.parse_args()

# The Nilakantha sequence is:
# 3 + 4/x*(x+1)*(x+2) - 4/(x+2)*(x+1+2)*(x+2+2) + 4/(x+4)*(x+1+4)*(x+2+4)...
# where x = 2

pi = 3
denominator_x = 2
add = True

for i in range(args.iterations):
    if add:
        pi += 4/(denominator_x * (denominator_x + 1) * (denominator_x + 2))
        add = False
    else:
        pi -= 4/(denominator_x * (denominator_x + 1) * (denominator_x + 2))
        add = True
    denominator_x += 2

print(pi)
