#!/bin/bash

# Copyright (C) 2019 Samuel First
# This file is part of benchmarks.

#     benchmarks is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     benchmarks is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.

iterations=$1

# The Nilakantha sequence is:
# 3 + 4/x*(x+1)*(x+2) - 4/(x+2)*(x+1+2)*(x+2+2) + 4/(x+4)*(x+1+4)*(x+2+4)...
# where x = 2

pi="3"
denominator=2
add=true

# Bash doesn't have floating point arithmetic, so we have to use awk
for i in $(seq 1 $iterations)
do
    if $add
    then
	pi="$(awk '{print $1+(4/($2*($2+1)*($2+2)))}' <<< $pi' '$denominator)"
	add=false
    else
	pi="$(awk '{print $1-(4/($2*($2+1)*($2+2)))}' <<< $pi' '$denominator)"
	add=true
    fi
    let "denominator += 2"
done

printf "%s\n" $pi
