;;;; Copyright (C) 2019 Samuel First
;;;; This file is part of benchmarks.
;;;;
;;;;     benchmarks is free software: you can redistribute it and/or modify
;;;;     it under the terms of the GNU General Public License as published by
;;;;     the Free Software Foundation, either version 3 of the License, or
;;;;     (at your option) any later version.
;;;; 
;;;;     benchmarks is distributed in the hope that it will be useful,
;;;;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;     GNU General Public License for more details.
;;;; 
;;;;     You should have received a copy of the GNU General Public License
;;;;     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.

(doList (arg *args*)
  (defvar *iterations* (parse-integer arg)))

;; The Nilakantha sequence is:
;; 3 + 4/x*(x+1)*(x+2) - 4/(x+2)*(x+1+2)*(x+2+2) + 4/(x+4)*(x+1+4)*(x+2+4)...
;; where x = 2

(defvar *pi* 3.0)
(defvar *denominator* 2)
(defvar *add* t)

(loop for i from 1 to *iterations* doing
     (if (equal *add* t)
	 (progn
	   (setf *pi* (+ *pi* (/ 4.0 (* *denominator*
					(+ *denominator* 1)
					(+ *denominator* 2)))))
	   (setf *add* nil))
	 (progn
	   (setf *pi* (- *pi* (/ 4.0 (* *denominator*
					(+ *denominator* 1)
					(+ *denominator* 2)))))
	   (setf *add* t)))
     (setf *denominator* (+ *denominator* 2)))

(format t "~f" *pi*)
