;;;; Copyright (C) 2019 Samuel First
;;;; This file is part of benchmarks.
;;;;
;;;;     benchmarks is free software: you can redistribute it and/or modify
;;;;     it under the terms of the GNU General Public License as published by
;;;;     the Free Software Foundation, either version 3 of the License, or
;;;;     (at your option) any later version.
;;;; 
;;;;     benchmarks is distributed in the hope that it will be useful,
;;;;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;     GNU General Public License for more details.
;;;; 
;;;;     You should have received a copy of the GNU General Public License
;;;;     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.

(doList (arg *args*)
  (defvar *iterations* (parse-integer arg)))

;; The Leibniz sequence is 1 - 1/n + 1/(n+2) - 1/(n+4)... where n = 3.
;; It calculates pi/4, so the end result must be multiplied by 4 to get pi

(defvar *pi* 1.0)
(defvar *denominator* 3)
(defvar *add* nil)

(loop for i from 1 to *iterations* doing
     (if (equal *add* t)
	 (progn
	   (setf *pi* (+ *pi* (/ 1.0 *denominator*)))
	   (setf *add* nil))
	 (progn
	   (setf *pi* (- *pi* (/ 1.0 *denominator*)))
	   (setf *add* t)))
     (setf *denominator* (+ *denominator* 2)))

(format t "~f" (* *pi* 4.0))
