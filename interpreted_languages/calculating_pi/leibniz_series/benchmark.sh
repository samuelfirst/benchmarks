#!/bin/bash

# Copyright (C) 2019 Samuel First
# This file is part of benchmarks.

#     benchmarks is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     benchmarks is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.

# First arg is the number of times to run the command
# Second arg is the number of iterations to pass the command
timesToRun=$1
iterations=$2

# Commands are stored as strings and eval'd later.  At first I
# was worried about the overhead created by eval, but after
# comparing a few runs of eval'd and non-eval'd commands, there
# wasn't a consistent or major difference.
commands[1]='python3 leibniz.py'
commands[2]='clisp leibniz.lisp'
commands[3]='bash leibniz.sh'

for n in $(seq 1 ${#commands[@]})
do
    averageReal='0'
    averageUser='0'
    averageSys='0'
    result=''
    for i in $(seq 1 $timesToRun)
    do
	output=$((time eval "${commands[n]}" $iterations) 2>&1)
	
	# Pull times and result out of output
	real=$(grep -e 'real' <<< $output | awk '{print $2}')
	user=$(grep -e 'user' <<< $output | awk '{print $2}')
	sys=$(grep -e 'sys' <<< $output | awk '{print $2}')
	result=$(echo $output | awk '{print $1}')

	# Change h/m/s to : for easier parsing
	real=$(sed -E 's/[hms]/:/g' <<< $real | sed -E 's/:$//')
	user=$(sed -E 's/[hms]/:/g' <<< $user | sed -E 's/:$//')
	sys=$(sed -E 's/[hms]/:/g' <<<  $sys | sed -E 's/:$//')

	# Convert everything to seconds and append to average
	real="$(awk -F ':' '{if (NF >= 3) {print ($NF+($(NF-1)*60)+($(NF-2)*3600))} else {print ($NF+($(NF-1)*60))}}' <<< $real)"
	user="$(awk -F ':' '{if (NF >= 3) {print ($NF+($(NF-1)*60)+($(NF-2)*3600))} else {print ($NF+($(NF-1)*60))}}' <<< $user)"
	sys="$(awk -F ':' '{if (NF >= 3) {print ($NF+($(NF-1)*60)+($(NF-2)*3600))} else {print ($NF+($(NF-1)*60))}}' <<< $sys)"

	averageReal="$(awk '{print $1+$2}' <<< $averageReal' '$real)"
	averageUser="$(awk '{print $1+$2}' <<< $averageUser' '$user)"
	averageSys="$(awk '{print $1+$2}' <<< $averageSys' '$sys)"
    done

    # Divide average by timesToRun and output
    printf "\ncommand: %s\noutput:  %s\n" "${commands[n]}" $result 
    awk '{print "average real: " ($1/$2)"s"}' <<< $averageReal' '$timesToRun
    awk '{print "average user: " ($1/$2)"s"}' <<< $averageUser' '$timesToRun
    awk '{print " average sys: " ($1/$2)"s"}' <<< $averageSys' '$timesToRun
done
