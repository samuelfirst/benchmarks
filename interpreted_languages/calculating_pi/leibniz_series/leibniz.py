#!/usr/bin/python3
'''
Copyright (C) 2019 Samuel First
This file is part of benchmarks.

    benchmarks is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    benchmarks is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with benchmarks.  If not, see <https://www.gnu.org/licenses/>.
'''
import argparse

# Get iterations from command line arg
parser = argparse.ArgumentParser()
parser.add_argument('iterations',
                    type=int,
                    help='Number of times to run sequence')
args = parser.parse_args()

# The Leibniz sequence is 1 - 1/n + 1/(n+2) - 1/(n+4)... where n = 3.
# It calculates pi/4, so the end result must be multiplied by 4 to get pi

pi = 1
denominator = 3
add = False

for i in range(args.iterations):
    if add:
        pi += 1/denominator
        add = False
    else:
        pi -= 1/denominator
        add = True
    denominator += 2

print(pi*4)
