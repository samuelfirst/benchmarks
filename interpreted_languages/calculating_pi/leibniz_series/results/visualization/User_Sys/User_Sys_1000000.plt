set boxwidth 0.5
set yrange [0:]
set linetype 1 lc rgb "blue"
set linetype 2 lc rgb "red"
set linetype 3 lc rgb "blue"
set linetype 4 lc rgb "red"
set style fill solid
set ylabel "Time (Seconds)"
set xlabel "Language"
set title "Runtime: User vs. Sys"
plot "User_Sys_1000000.dat" using 1:3:($1+1):xtic(2) with boxes linecolor variable title "", (x=-1) with boxes lc rgb "blue" t "User", (x=-2) with boxes lc rgb "red" t "Sys"