# User/System Time Utilization

These are bar charts showing the difference between the time spent in userland
and the time spent in system calls.

## Notes

* The results for bash are not included for 10000 iterations on up.  This is
  because it throws off the results for python and common lisp
